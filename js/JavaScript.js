﻿
hideSections();
showStart();

function showStart() {

    $("body").hide().fadeIn(1000);


}
function unhide() {
    $("p").hide().fadeIn(1500);
}

function hideSections() {
    $("section").hide();
}

function showAbout() {
    if ($("#asideHeader").is(":visible"))
    {
        $("#asideHeader").slideUp(500);
    }

    if ($(".sectionContact").is(":visible"))
    {
            $(".sectionContact").slideUp(500);
     }
    if ($(".sectionDreams").is(":visible"))
    {
            $(".sectionDreams").slideUp(500);
        }
    if ($(".sectionChallenges").is(":visible"))
    {
            $(".sectionChallenges").slideUp(500);
    }

    if ($(".sectionAbout").is(":hidden")) {
        $(".sectionAbout").slideDown(500);
    } else {
        $(".sectionAbout").slideUp(500);
        $("#asideHeader").slideDown(500);
    }
    
}


function showChallenges() {
    if ($("#asideHeader").is(":visible"))
    {
        $("#asideHeader").slideUp(500);
    }

    if ($(".sectionContact").is(":visible"))
    {
        $(".sectionContact").slideUp(500);
    }
if ($(".sectionDreams").is(":visible"))
{
            $(".sectionDreams").slideUp(500);
        }
if ($(".sectionAbout").is(":visible"))
{
            $(".sectionAbout").slideUp(500);
        }
    if ($(".sectionChallenges").is(":hidden")) {
        $(".sectionChallenges").slideDown(500);
} else {
        $(".sectionChallenges").slideUp(500);
    $("#asideHeader").slideDown(500);
}   
}

function showDreams() {

    if ($("#asideHeader").is(":visible"))
    {
        $("#asideHeader").slideUp(500);
    }
    if ($(".sectionContact").is(":visible"))
    {
            $(".sectionContact").slideUp(500);
        }
    if ($(".sectionAbout").is(":visible"))
    {
            $(".sectionAbout").slideUp(500);
        }
    if ($(".sectionChallenges").is(":visible"))
    {
            $(".sectionChallenges").slideUp(500);
    }

    if ($(".sectionDreams").is(":hidden")) {
        $(".sectionDreams").slideDown(500);
    } else {
        $(".sectionDreams").slideUp(500);
        $("#asideHeader").slideDown(500);
    }
    
}

function showContact() {
    if ($("#asideHeader").is(":visible"))
    {
        $("#asideHeader").slideUp(500);
    }

    if ($(".sectionAbout").is(":visible"))
    {
            $(".sectionAbout").slideUp(500);
        }
    if ($(".sectionDreams").is(":visible"))
    {
        $(".sectionDreams").slideUp(500);
        }
    if ($(".sectionChallenges").is(":visible"))
    {
            $(".sectionChallenges").slideUp(500);
    }

    if ($(".sectionContact").is(":hidden")) {
        $(".sectionContact").slideDown(500);
    } else {
        $(".sectionContact").slideUp(500);
        $("#asideHeader").slideDown(500);
    }
  
}

function retard() {
    var obrazek = $("#retardator");
    var napis = $("#napis");

    napis
        .animate({ left: "-3em" }, 150)
        .animate({ left: "1em" }, 150);

    obrazek
        .animate({ top: "-3em" }, 150)
        .animate({ top: "1em" }, 150);

}


